let users = [
    'Luis',
    'Andres',
    'Martin',
    'Yeyo',
    'Jesus',
    'Karen',
    'Ismael',
    'Noe',
    'Felipe',
    'Cesar',
];

const filteringAndSorting = () => {
    const filteringAndSortingKeyWords = users
        .filter((name, index) => users.lastIndexOf(name) === index)
        .sort((a, b) => (a < b ? -1 : 1));
    console.log('Filtering and Sorting a List of Strings');
    console.log('Array original: ', users);
    console.log('Array filtered and sorted');
    console.table(filteringAndSortingKeyWords);
};

// let mensajes = [
//     'Mensaje 1',
//     'Mensaje 2',
//     'Mensaje 3',
//     'Mensaje 4',
//     'Mensaje 5',
//     'Mensaje 6',
//     'Mensaje 7',
//     'Mensaje 8',
//     'Mensaje 9',
//     'Mensaje 10',
// ];

// const miDependencia = () => {
//     const mensaje = mensajes[Math.floor(Math.random() * mensajes.length)];
//     console.log(`\x1b[34m${mensaje}\x1b[89m`);
// };

module.exports = {
    filteringAndSorting,
    // miDependencia,
};
